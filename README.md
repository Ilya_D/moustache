### Робот-жук

#### Навигация по проекту

- [CAD детали](/content/CAD)
- [Изображения](/content/foto)
- [Схема](/content/schema)
- [Симулятор](/content/SimulIde)

#### Модель

***
![Logo](/content/foto/1.png)
![Logo](/content/foto/2.png)
![Logo](/content/foto/3.png)
![Logo](/content/foto/4.png)
![Logo](/content/foto/5.png)
![Logo](/content/foto/6.png)
![Logo](/content/foto/7.png)
***
#### Принципиальная схема
![Logo](/content/foto/scheme.png)

### Принцип работы

![Logo](https://gitlab.com/Ilya_D/moustache/-/raw/main/content/SimulIde/movie.gif?ref_type=heads)

#### Габариты мотора F130
![Logo](/content/foto/F130.png)

#### Микропереключатель SM5-03P
![Logo](/content/foto/SM5-03P.png)

#### Страница колёсного робота

<https://gitflic.ru/project/rurewa/car-bot>